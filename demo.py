from fsstaticvars.static import STATIC_ENV_VAR
from fsstaticvars.static import ANOTHER_STATIC_VAR

if __name__ == '__main__':
    print(STATIC_ENV_VAR)
    print(ANOTHER_STATIC_VAR)
