import os

SOME_STATIC_VAR = 123
ANOTHER_STATIC_VAR = 'blub'
STATIC_ENV_VAR = os.environ.get('PYTHONPATH')
ANOTHER_STATIC_ENV_VAR = os.environ.get('BLUB', 'default string value')


class SomeClass:
    var_a = 1
    var_b = 2

    def __init__(self, text='blub'):
        self._var_c = text

    def get_var(self, var_name):
        return getattr(self, var_name)

    @property
    def var_c(self):
        return self._var_c
