from .static import SomeClass


def get_class(text):
    """
    Returns a class instance of ``SomeClass``
    Args:
        text (str): Variable C text value

    Returns:
        SomeClass:
            Instance of ``SomeClass``

    """
    return SomeClass(text)


if __name__ == '__main__':
    f = get_class('qwe')
