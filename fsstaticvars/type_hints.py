from .static import SomeClass


def get_class(text: str) -> SomeClass:
    return SomeClass(text)


if __name__ == '__main__':
    f = get_class('qwe')
