from .static import SOME_STATIC_VAR
from .static import ANOTHER_STATIC_ENV_VAR

if __name__ == '__main__':
    print(SOME_STATIC_VAR)
    print(ANOTHER_STATIC_ENV_VAR)
